/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.redamm.atun.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author abraham
 */
@Embeddable
public class Gateway implements Serializable {

    public static final String GTW_PAYPAL = "paypal";
    public static final int    ID_PAYPAL=1 ;

    public Gateway() {
    }

    public Gateway(String name) {
        this.name = name;
    }
    
    @Column(name="NAME")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
        
}
