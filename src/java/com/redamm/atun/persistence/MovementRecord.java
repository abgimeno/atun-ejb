/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redamm.atun.persistence;

import com.redamm.payment.State;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;

/**
 *
 * @author abraham
 * 
 */
@NamedQueries({
    @NamedQuery(name = "MovementRecord.getRecordByRefererId", query = "SELECT r FROM MovementRecord r WHERE r.orderId =:orderId")
})
@Entity
@Table(name = "MOVEMENT")
public class MovementRecord implements Serializable {

    @TableGenerator(name = "movementAtunGen", table = "SEQUENCE_TABLE",
                    pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE",
                    pkColumnValue = "MOVEMENTID", allocationSize = 1, initialValue = 1)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "movementAtunGen")
    @Column(name = "MOVEMENTID")   //our unique identifier (webdepagos)
    private int movementId;
    
    @Embedded
    private Gateway gateway;
    
    @Embedded
    private State movState = new State(State.STATE_UNKNOWN);
    
    @Embedded
    private Concern concern;
    
    @Column(name = "ORDERID", nullable = false)  //orderId in the shop (shark, manta)
    private String orderId;
    
    @Column(name = "USERID", nullable = false) //userId 
    private String userId;
    
    @Column(name = "TRANSACTIONID", nullable= false, unique=true)  //PAYPAL'S OR VENDOR's NUMBER
    private String transactionId;
    
    @Column(name = "AMOUNT")
    private double amount;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "MOVEMENTDATE")
    private Date date;

    public Concern getConcern() {
        return concern;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setConcern(Concern concern) {
        this.concern = concern;
    }

    public int getMovementId() {
        return movementId;
    }

    public void setMovementId(int movementId) {
        this.movementId = movementId;
    }

    public State getMovState() {
        return movState;
    }

    public void setMovState(State movState) {
        this.movState = movState;
    }

    public int getState() {
        
        if (movState != null) {
            return movState.getState();
        } else {
            movState = new State(State.STATE_UNKNOWN);
        }
        return movState.getState();
    }

    public void setState(int movState) {
        this.movState = new State(movState);
    }

    public Gateway getGateway() {
        return gateway;
    }

    public void setGateway(Gateway gateway) {
        this.gateway = gateway;
    }
}
