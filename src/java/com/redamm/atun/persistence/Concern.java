/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.redamm.atun.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abraham
 */
@Embeddable
public class Concern implements Serializable{

    public Concern() {
    }

    public Concern(String domainName) {
        this.domainName = domainName;
    }

  
    @Column(name="domainName")
    private String domainName;
       
    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }
    
}   
