/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.redamm.atun.business;


import com.redamm.atun.persistence.MovementRecord;
import javax.ejb.Local;

/**
 *
 * @author abraham
 */
@Local
public interface MovementManager {
        
    public MovementRecord getMovement(String movementId);
    public MovementRecord mergeMovement(MovementRecord movement);
    public MovementRecord persistMovement(MovementRecord movement);
    public MovementRecord geMovementByRefererId(String refererId);
}   
