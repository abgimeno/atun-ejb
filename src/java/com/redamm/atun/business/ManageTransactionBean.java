/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.redamm.atun.business;

import com.redamm.atun.persistence.MovementRecord;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import org.apache.log4j.Logger;

/**
 *
 * @author abraham
 * 
 */
@Stateless(name = "ManageTransaction", mappedName = "ejb/ManageTransaction")
public class ManageTransactionBean implements ManageTransaction {

    @Resource(mappedName = "jms/QueueFactory")
    private QueueConnectionFactory connectionFactory;
    @Resource(mappedName = "jms/MantaTrans")        
    private Destination manta;
    
    
    private Connection connection;
    private Session session;
    private MessageProducer producer;

    @PostConstruct
    public void init() {
        try {
            Logger.getLogger(ManageTransactionBean.class).info("Creating connections");
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer(manta);
        } catch (JMSException ex) {
            Logger.getLogger(ManageTransactionBean.class).error("ManageTransactionBean::init()", ex);
        }
    }

    public void notifyTransaction(String referer, MovementRecord mov) {
        try {
            Logger.getLogger(ManageTransactionBean.class).info("Starting Notifying");
            ObjectMessage message = session.createObjectMessage();
            MovementRecord r = mov;
            message.setObject(r);
            producer.send(message);

        } catch (JMSException ex) {
            Logger.getLogger(ManageTransactionBean.class).error("ManageTransactionBean::notifyTransaction()", ex);
        }
    }

    @PreDestroy
    public void finish() {
        try {
            Logger.getLogger(ManageTransactionBean.class).info("Destroying connections");
            connection.close();
            session.close();
        } catch (JMSException ex) {
            Logger.getLogger(ManageTransactionBean.class).error("ManageTransactionBean::finish()", ex);
        }
    }
}
