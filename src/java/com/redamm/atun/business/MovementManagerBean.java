/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.redamm.atun.business;

import com.redamm.atun.persistence.MovementRecord;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author abraham
 */

@Stateless(name="MovementManager", mappedName="ejb/MovementManager")
public class MovementManagerBean implements MovementManager{
    
    @PersistenceContext(unitName = "atun-ejbPU")
    protected EntityManager atunEM;

   
    /** Default logger **/
    protected static Logger logger;
    
    public MovementRecord getMovement(String movementId) {
        return atunEM.find(MovementRecord.class, movementId);
    }

    public MovementRecord mergeMovement(MovementRecord movement) {
        return atunEM.merge(movement);
    }

    public MovementRecord persistMovement(MovementRecord movement) {
        atunEM.persist(movement);
        return movement;
    }

    public MovementRecord geMovementByRefererId(String refererId) {
        return (MovementRecord) atunEM.createNamedQuery("MovementRecord.getRecordByRefererId")
                      .setParameter("orderId",refererId)
                      .getSingleResult();
    }
            
    
}
