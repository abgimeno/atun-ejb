/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.redamm.atun.business;

import com.redamm.atun.persistence.MovementRecord;

/**
 *
 * @author abraham
 */
public interface ManageTransaction {

    public void notifyTransaction(String referer, MovementRecord mov);
}
